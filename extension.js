const { execFileSync } = require('child_process');
const {
  window,
  StatusBarAlignment,
  workspace,
  commands,
  Uri
} = require('vscode');

const request = require('request-promise-native');
const moment = require('moment');
const gitUrlParser = require('./git-url-parser');

let stopMonitoring = null;
let lastPipeline = null;

const createGitLabClient = (apiUrl, project, token) => {
  const url = _ => `${apiUrl}/projects/${encodeURIComponent(project)}${_}`;

  return endpoint =>
    request({
      uri: url(endpoint),
      json: true,
      headers: {
        'PRIVATE-TOKEN': token
      },
      rejectUnauthorized: workspace.getConfiguration(`http`)['proxyStrictSSL'],
      resolveWithFullResponse: true,
      simple: false
    })
      .then(x => {
        if (x.statusCode <= 400) return x;
        else throw new Error(`${x.body.message}`);
      })
      .then(response => response.body);
};

const createStatusBarItem = () => {
  const item = window.createStatusBarItem(StatusBarAlignment.Left, Infinity);
  item.show();
  item.command = 'pipeline.openInBrowser';
  return Object.assign({}, item, {
    showText: txt => {
      item.text = `$(git-commit) pipeline: ${txt}`;
    }
  });
};

const gitClient = ws => (...args) =>
  execFileSync('git', [`--git-dir`, `${ws.uri.fsPath}/.git/`, ...args])
    .toString()
    .trim();

const repo = () =>
  new Promise((resolve, reject) => {
    try {
      const ws = workspace.workspaceFolders[0];
      const git = gitClient(ws);

      const branch = git('rev-parse', '--abbrev-ref', 'HEAD').trim();
      const url = git('config', '--get', 'remote.origin.url');

      const { domain, project } = gitUrlParser(url);

      const info = {
        domain,
        project,
        branch: branch === 'HEAD' ? 'master' : branch // HEAD if it is a tag instead of a branch
      };
      resolve(info);
    } catch (err) {
      console.log(err);
      reject(err);
    }
  });

const migrateV1toV2 = userSettings =>
  new Promise((resolve, reject) => {
    if (
      userSettings.has('tokens') ||
      userSettings.has('notifyOnFailed') ||
      userSettings.has('interval')
    ) {
      const tokens = userSettings.get('tokens') || {};
      const interval = userSettings.get('interval') || 10000;
      const notifyOnFailedSettings = userSettings.get('notifyOnFailed') || {};

      const configV2 = Object.keys(tokens).reduce(
        (cfg, domain) => ({
          ...cfg,
          [domain]: {
            token: tokens[domain],
            notifyOnFailed: notifyOnFailedSettings[domain] || false,
            interval
          }
        }),
        {}
      );
      console.log(
        'Please migrate the configuration to the new format descibred here: https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor'
      );
      resolve(configV2);
    }
    resolve(userSettings);
  });

const getExtensionSettings = async domain => {
  const defaultSettings = {
    token: null,
    interval: 10000,
    notifyOnFailed: false,
    apiUrl: `https://${domain}/api/v4`,
    version: 2
  };
  const config = workspace.getConfiguration(`gitlabPipelineMonitor`);
  const settings = await migrateV1toV2(config);
  const usersettings = { ...defaultSettings, ...settings[domain] };
  return usersettings;
};

const getLatestPipeline = async ({
  domain,
  project,
  branch,
  token,
  apiUrl
}) => {
  if (!token) throw new Error(`No token for '${domain}'`);
  const { host, protocol } = require('url').parse(apiUrl);
  const getData = createGitLabClient(apiUrl, project, token);
  const pipelines = await getData(`/pipelines/?ref=${branch}`);
  if (!pipelines || pipelines.length === 0)
    throw new Error(`No pipeline found`);
  const [{ id }] = pipelines;
  const pipeline = await getData(`/pipelines/${id}`);
  return pipeline;
};

const createText = ({ status, finished_at, ref }) => {
  const time = finished_at ? moment(finished_at).fromNow() : '';
  return `${status.toUpperCase()} on '${ref}' ${time}`.trim();
};

const getVsCodeSymbol = status =>
  ({
    success: '$(check)',
    failed: '$(x)',
    running: '$(triangle-right)',
    pending: '$(clock)'
  }[status] || '');

const updateMonitor = async (settings, showText) => {
  try {
    const repository = await repo();
    const config = { ...settings, ...repository };
    const pipeline = await getLatestPipeline(config);
    const text = createText(pipeline);
    const symbol = getVsCodeSymbol(pipeline['status']);
    showText(`${symbol} ${text}`.trim());

    if (
      config.notifyOnFailed === true &&
      pipeline.status === 'failed' &&
      JSON.stringify(lastPipeline) !== JSON.stringify(pipeline)
    ) {
      window.showErrorMessage(`pipeline: ${text}`);
    }
    lastPipeline = pipeline;
  } catch (err) {
    showText(`$(alert) ${err.message}`);
    console.log(err);
  }
};

const registerExtension = context => {
  const cmd = commands.registerCommand('pipeline.openInBrowser', () => {
    if (lastPipeline === null) return;
    commands.executeCommand('vscode.open', Uri.parse(lastPipeline.web_url));
  });
  context.subscriptions.push(cmd);

  const item = createStatusBarItem();
  context.subscriptions.push(item);

  const { showText } = item;
  return showText;
};

exports.activate = async context => {
  const showText = registerExtension(context);
  showText('...');

  const repository = await repo();
  const settings = await getExtensionSettings(repository[`domain`]);

  await updateMonitor(settings, showText);
  const loop = setInterval(async () => {
    await updateMonitor(settings, showText);
  }, parseInt(settings['interval']));
  stopMonitoring = () => clearInterval(loop);
};

exports.deactivate = () => {
  if (stopMonitoring !== null) stopMonitoring();
};
